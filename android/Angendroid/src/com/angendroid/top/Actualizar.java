package com.angendroid.top;

import java.util.ResourceBundle;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Actualizar extends Activity {
	
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	private SQLiteDatabase bd;
	EditText TextNombreA;
	EditText TextTelefonoA;
	EditText TextCorreoA;
	EditText TextDescripcionA;
	private Button botActualizar;
	private Button botMail;
	
	final String crearTablaContacto = "create table if not exists "  
		  + " contacto (codigo integer primary key autoincrement, "  
		  + " nombre text not null, telefono text not null unique, "
		  + " correo text not null, descripcion text not null);";
	
	//ResourceBundle para lectura de properties...
    ResourceBundle rb = ResourceBundle.getBundle("assets.appMessages");
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		Log.d(Constantes.DEBUG, "Inicio -- onCreate()");
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.actualizar);
		
		TextNombreA = (EditText)findViewById(R.id.TextNombreA);
		
		Bundle extras = getIntent().getExtras();
		 //nombre = "";
		if(extras!=null){
			TextNombreA.setText(extras.getString("nombre"));
		}	
		
		TextTelefonoA = (EditText)findViewById(R.id.TextTelefonoA);
		TextCorreoA = (EditText)findViewById(R.id.TextCorreoA);
		TextDescripcionA = (EditText)findViewById(R.id.TextDescripcionA);
		botActualizar = (Button)findViewById(R.id.botActualizar);
		botMail = (Button)findViewById(R.id.botMail);
		
		
		bd = openOrCreateDatabase("agenda",SQLiteDatabase.OPEN_READWRITE, null);
        bd.execSQL(crearTablaContacto); 
		Cursor c = bd.rawQuery("SELECT * FROM contacto WHERE nombre='"+TextNombreA.getText().toString()+"'", null);
		
		try{
			if(c!=null){
				int i = c.getColumnIndexOrThrow("telefono");
				int j = c.getColumnIndexOrThrow("correo");
				int k = c.getColumnIndexOrThrow("descripcion");
				
				while(c.moveToNext()){
	            	//Recorremos el cursor hasta que no haya más registros
	            	//telefono = a.getString(i);
	                TextTelefonoA.setText(c.getString(i));
	                TextCorreoA.setText(c.getString(j));
	                TextDescripcionA.setText(c.getString(k));
	            }
			}
		}
		catch(Exception e){
			Toast.makeText(getApplicationContext(), 
        			"c=null: "+e, Toast.LENGTH_LONG).show();
		}
		
		botActualizar.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View arg0) {
				
				Log.d(Constantes.DEBUG, "Inicio -- botActualizar.setOnClickListener()");
				
				// TODO Auto-generated method stub
				try{
				bd.execSQL("UPDATE contacto SET telefono='"+TextTelefonoA.getText().toString()+"',correo='"+TextCorreoA.getText().toString()+"',descripcion='"+TextDescripcionA.getText().toString()+"' WHERE nombre='"+TextNombreA.getText().toString()+"'");
				Toast.makeText(getApplicationContext(), 
		        		  rb.getString("contacto.actualizado.correctamente"), Toast.LENGTH_LONG).show();
				}
				catch(Exception ex){
				Toast.makeText(getApplicationContext(), 
		        		  rb.getString("no.se.actualizo.correctamente"), Toast.LENGTH_LONG).show();
				}
				
				Intent main = new Intent(Actualizar.this,Principal.class);
	    		startActivity(main);
			
	    		Log.d(Constantes.DEBUG, "Fin -- botActualizar.setOnClickListener()");
	    		
			}
		});
		
		
		botMail.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				
				Log.d(Constantes.DEBUG,"Inicio -- botMail.setOnClickListener()");
				
				Mail m = new Mail("ant.peiro@gmail.com", "anmarpei5e6b640"); 
				 
			      String[] toArr = {"ant.peiro@gmail.com", "anmarpei@gmail.com"}; 
			      m.setTo(toArr); 
			      m.setFrom("ant.peiro@gmail.com"); 
			      m.setSubject("This is an email sent using my Mail JavaMail wrapper from an Android device."); 
			      m.setBody("Email body."); 
			 
			      try { 
			        //m.addAttachment("/sdcard/filelocation");
			 
			        if(m.send()) { 
			          Toast.makeText(Actualizar.this, "Email was sent successfully.", Toast.LENGTH_LONG).show(); 
			        } else { 
			          Toast.makeText(Actualizar.this, "Email was not sent.", Toast.LENGTH_LONG).show(); 
			        } 
			      } catch(Exception e) { 
			        //Toast.makeText(MailApp.this, "There was a problem sending the email.", Toast.LENGTH_LONG).show(); 
			        Log.e("MailApp", "Could not send email", e); 
			      } 
			      
			      
				Intent main = new Intent(Actualizar.this,Principal.class);
	    		startActivity(main);
	    		
	    		Log.d(Constantes.DEBUG,"Fin -- botMail.setOnClickListener()");
	    		
			}
			
		});
		
		Log.d(Constantes.DEBUG,"Fin -- onCreate()");
		
	}
}
