package com.angendroid.top;

public class Constantes {

	//Niveles de logging, de mayor (poco detalle) a menor (mucho detalle)
	public static final String OFF = "OFF";
	public static final String FATAL = "FATAL";
	public static final String ERROR = "ERROR";
	public static final String WARN = "WARN";
	public static final String INFO = "INFO";
	public static final String DEBUG = "DEBUG";
	public static final String TRACE = "TRACE";
	public static final String ALL = "ALL";
}
