package com.angendroid.top;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xmlpull.v1.XmlSerializer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Principal extends Activity {

	/** Called when the activity is first created. */
		
	private SQLiteDatabase bd;
	private Button botagregar;
	private Button botInternet;
	private Button botCerrar;
	public ListView lstMostrar;
	public int posicion;
	
	final ArrayList<String> lista = new ArrayList<String>();
	String usuario, telefono;
	private static final String mostrarContactos = "SELECT nombre,telefono FROM contacto";
	final String crearTablaContacto = "create table if not exists "  
		  + " contacto (codigo integer primary key autoincrement, "  
		  + " nombre text not null, telefono text not null unique, "
		  + " correo text not null, descripcion text not null);";
	
	//ResourceBundle para lectura de properties...
    ResourceBundle rb = ResourceBundle.getBundle("assets.appMessages");
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		
		Log.d(Constantes.DEBUG,"Inicio -- onCreate()");
		
		super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        botagregar = (Button) findViewById(R.id.butAgregar);    
		lstMostrar = (ListView)findViewById(R.id.listVMostrar);
		botInternet = (Button)findViewById(R.id.butInternet);
		botCerrar = (Button) findViewById(R.id.butSalir);
		
		bd = openOrCreateDatabase("agenda",SQLiteDatabase.OPEN_READONLY, null);
        bd.execSQL(crearTablaContacto); 
        	
        Cursor c = bd.rawQuery(mostrarContactos, null); 
        try{
        	if(c!=null){
	        	
        		int i = c.getColumnIndexOrThrow("nombre");
        		//int j = c.getColumnIndexOrThrow("telefono");
	            
	            //Nos aseguramos de que existe al menos un registro
	            while(c.moveToNext()){
	            	//Recorremos el cursor hasta que no haya más registros
	            	usuario = c.getString(i); 
	                lista.add(usuario);
	            }
        	}
        	else 
        		Toast.makeText(getApplicationContext(), 
  	        		  rb.getString("no.hay.nada"), Toast.LENGTH_LONG).show();
        }
        catch (Exception e){
        	Log.i(Constantes.INFO,rb.getString("error.al.abrir.o.crear.la.base.de.datos") + e); 
        }
        	
        if(bd!=null)
           	bd.close();
                        
        ListAdapter adapter = new ArrayAdapter<String>(this,
                   android.R.layout.simple_list_item_single_choice, lista);
            
        lstMostrar.setAdapter(adapter);
              
        lstMostrar.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View a, int position,
					long id) {
				// TODO Auto-generated method stub
				posicion = position;
				lstMostrar.setItemChecked(position, true);
				Toast.makeText(getApplicationContext(), 
	  	        		  rb.getString("seleccionaste")+lista.get(position), Toast.LENGTH_LONG).show();
				
				//Llamamos a modificar...
				Intent modificar = new Intent(Principal.this, Actualizar.class);
				modificar.putExtra("nombre", lista.get(posicion).toString());
				startActivity(modificar);
				
			}
        	
		});
        
        botagregar.setOnClickListener(new OnClickListener(){
        	
        	public void onClick(View v){
        		Intent agregar = new Intent(Principal.this,AgregarContacto.class);
        		startActivity(agregar);
        	}
        });
        
        
        botInternet.setOnClickListener(new View.OnClickListener()
        {
        	public void onClick(View v)
        	{
        		
        		try {
        			
        			String sFichero = "g:\fichero.txt";
        			File fichero = new File(sFichero);
        			
        			if (fichero.exists()){
        				  System.out.println("El fichero " + sFichero + " existe");
        			}else{
        				  System.out.println("Pues va a ser que no");
        			}//fin else
        			
        			
        			
        			
        			//String sUrl = "http://www.google.com";
        			String searchType = "title";
        			String searchText = "Ben-Hur";
        			
        			String sT = searchText.toLowerCase().replace("-", "+");
        			
        			String sUrl = "http://www.filmaffinity.com/es/search.php?stext="+sT+"&stype="+searchType;
	     			/*
        			URL url = new URL(sUrl);
	     			URLConnection urlCon = url.openConnection();
     			   
     			   
     			    String html = readFromBuffer(new BufferedReader(new InputStreamReader(
     					 urlCon.getInputStream())));
     			   
     			    System.out.println(html);
     			    */
        			
        			Document doc = Jsoup.connect(sUrl).get();
        			Elements newsHeadlines = doc.select("#mp-itn b a");
        			
        			//Obtengo los enlaces con href con extensi�n .html ...
        			Elements links = doc.select("a[href$=.html]");
        			
        			Elements imports = doc.select("link[href]");
        			
        			System.out.println("\nImports: " + imports.size());
        	        for (Element link : links) {
        	            System.out.println("TagName: "+link.tagName()+" LinkAttribute: "+link.attr("abs:href"));
        	        
        	            /**
        	             * INICIO -- codigo por probar
        	             */
        	            if(link.attr("abs:href").equals("http://www.filmaffinity.com/es/film800123.html")){
        	            	String sUrl2 = "http://www.filmaffinity.com/es/film800123.html";
        	            	
        	            	System.out.println("INICIO - Tirabuz�n");
        	            	
        	            	Document doc2 = Jsoup.connect(sUrl2).get();
        	            	
        	            	
//        	            	String sFichero2 = "c:\fichero.txt";
//        	            	File ficheroFile = new File(sFichero2);
        	            	 
//        	            	BufferedWriter bw = new BufferedWriter(new FileWriter(sFichero2));
//        	            	bw.write(doc2.text());
//        	            	bw.close();
//        	            	
        	            	/**
        	            	 * INICIO -- Escritura de fichero
        	            	 */
        	            	
        	            	try
        	            	{
        	            		//Creamos el serializer
        	        			XmlSerializer ser = Xml.newSerializer();
        	        			 
        	        			//Creamos un fichero en memoria interna
        	        			OutputStreamWriter fout =
        	        			    new OutputStreamWriter(
        	        			        openFileOutput("contenido.xml",
        	        			            Context.MODE_PRIVATE));
        	        			 
        	        			//Asignamos el resultado del serializer al fichero
        	        			ser.setOutput(fout);
        	        			 
        	        			//Construimos el XML
        	        			ser.startTag("", "filmContent");
        	        			 
        	        			ser.text(doc2.text());
        	        			 
//        	        			ser.startTag("", "apellidos");
//        	        			ser.text("ApellidosUsuario1");
//        	        			ser.endTag("", "apellidos");
        	        			
        	        			
        	        			
        	        			ser.endTag("", "filmContent");
        	        			 
        	        			ser.endDocument();
        	        			 
        	        			fout.close();
        	            	}
        	            	catch (Exception ex)
        	            	{
        	            	    Log.e("Ficheros", "Error al escribir fichero a memoria interna");
        	            	}
        	            	
        	            	/**
        	            	 * FIN -- Escritura de fichero
        	            	 */
        	            	
        	            	/**
        	            	 * INICIO -- Lectura de fichero
        	            	 */
//        	            	try
//        	            	{
//        	            	    BufferedReader fin =
//        	            	        new BufferedReader(
//        	            	            new InputStreamReader(
//        	            	                openFileInput("prueba_int.txt")));
//        	            	 
//        	            	    String texto = fin.readLine();
//        	            	    fin.close();
//        	            	}
//        	            	catch (Exception ex)
//        	            	{
//        	            	    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
//        	            	}
        	            	/**
        	            	 * FIN -- Lectura de fichero
        	            	 */
        	            	
        	            	Elements sinopsis = doc2.getElementsByTag("tr");
        	            	//Elements contenidoSinopsis = doc2.select("td");
        	            	for (Element element : sinopsis) {
								
        	            		
        	            		
        	            		System.out.println(element.text());
//								Element sinopsisIntro = element.after(element);
//								System.out.println("Inicio - Sinopsis");
//								System.out.println(sinopsisIntro.text());
//								System.out.println("Fin - Sinopsis");
							}//fin for
        	            	
        	            	System.out.println("FIN - Tirabuz�n");
        	            	
        	            }//fin if
        	            /**
        	             * FIN -- codigo por probar
        	             */
        	            
        	        }
        			
        			
     			    //String argumento = filmProcess(html,searchText);
     			    
     			       			  
     			} 
        		catch (Exception e) 
     			{
        			e.printStackTrace();
     			}
        		
        	}
        });
        
        
        botCerrar.setOnClickListener(new View.OnClickListener() 
        {
          public void onClick(View v) 
          {
        	  finish();
          }
        });  
        
        Log.d(Constantes.DEBUG,"Fin -- onCreate()");
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		Log.d(Constantes.DEBUG,"Inicio -- onCreateOptionsMenu()");
		
        //Alternativa 1
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menutel, menu);
        
        Log.d(Constantes.DEBUG,"Fin -- onCreateOptionsMenu()");
        
        return true;
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		Log.d(Constantes.DEBUG,"Inicio -- onOptionsItemSelected()");
		
	    switch (item.getItemId()) {
	        case R.id.MnuLlamar:{
	            bd = openOrCreateDatabase("agenda",SQLiteDatabase.OPEN_READONLY, null);
	            bd.execSQL(crearTablaContacto);
	            String nombre = lista.get(posicion).toString();
	            
	            try{
	            	Cursor a = bd.rawQuery("SELECT telefono from contacto where nombre = '"+nombre+"'", null);
	            	
	            	if(a!=null){
	            		int i = a.getColumnIndexOrThrow("telefono");
	    	            
	    	            //Nos aseguramos de que existe al menos un registro
	    	            while(a.moveToNext()){
	    	            	//Recorremos el cursor hasta que no haya más registros
	    	            	telefono = a.getString(i);
	    	            }
	            		
	            		//Mostrar un mensaje de confirmación antes de realizar la llamada  
	                	AlertDialog.Builder alertDialog = new AlertDialog.Builder(Principal.this);
	                    alertDialog.setMessage(rb.getString("desea.realizar.la.llamada.al.contacto"));
	                    alertDialog.setTitle(rb.getString("llamar.al.contacto"));
	                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
	                    alertDialog.setCancelable(false);
	                    alertDialog.setPositiveButton(rb.getString("si"), new DialogInterface.OnClickListener() 
	                    {
	                      public void onClick(DialogInterface dialog, int which) 
	                      {
	                        try 
	                        {          	
	                          String number = rb.getString("tel") + telefono.toString().trim(); 	
	                          Toast.makeText(getApplicationContext(), 
	                        		  rb.getString("llamando.al") + telefono, Toast.LENGTH_LONG).show();
	                          Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number)); 
	                          startActivity(callIntent);
	                        } 
	                        catch (Exception e) 
	                        {
	                        	Toast.makeText(getApplicationContext(), 
	                        			rb.getString("no.se.ha.podido.realizar.la.llamada")+e, Toast.LENGTH_LONG).show();
	                        }
	                      } 
	                    }); 
	                    alertDialog.setNegativeButton(rb.getString("no"), new DialogInterface.OnClickListener() 
	                    {
	                      public void onClick(DialogInterface dialog, int which) 
	                      {
	                        Toast.makeText(getApplicationContext(), 
	                        		rb.getString("llamada.cancelada"), Toast.LENGTH_LONG).show();
	                      } 
	                    }); 
	                    alertDialog.show();
	            	}
	            	else
	            		Toast.makeText(getApplicationContext(), 
			  	        		  rb.getString("consulta.mal.hecha"), Toast.LENGTH_LONG).show();
	            }
	            catch(Exception ex){
	            	Log.i(Constantes.INFO,rb.getString("error.al.abrir.o.crear.la.base.de.datos") + ex); 
	            }
	            if(bd!=null)
	            	bd.close();
	            
	            Log.d(Constantes.DEBUG,"Fin -- onOptionsItemSelected()");
	            
	            return true;
	        }
	        case R.id.MnuActualizar:{
	        	Intent actualizar = new Intent(Principal.this,Actualizar.class);
	        	actualizar.putExtra("nombre", lista.get(posicion).toString());
        		startActivity(actualizar);
        		
        		Log.d(Constantes.DEBUG,"Fin -- onOptionsItemSelected()");
        		
	            return true;
	        }
	        case R.id.MnuEliminar:{
	        	bd = openOrCreateDatabase("agenda",SQLiteDatabase.OPEN_READWRITE, null);
	            bd.execSQL(crearTablaContacto);
	            final String nombre = lista.get(posicion).toString();
	            
	        	//Mostrar un mensaje de confirmación antes de eliminar la base de datos  
	          	AlertDialog.Builder alertDialog = new AlertDialog.Builder(Principal.this);
	          	alertDialog.setMessage(rb.getString("desea.eliminar.a.este.contacto"));
	          	alertDialog.setTitle(rb.getString("eliminar.contacto"));
	          	alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
	          	alertDialog.setCancelable(false);
	          	alertDialog.setPositiveButton(rb.getString("si"), new DialogInterface.OnClickListener() 
	              {
	                public void onClick(DialogInterface dialog, int which) 
	                {
	                  try 
	                  {  
	                      Toast.makeText(getApplicationContext(), 
	                    		  rb.getString("eliminando.contacto") + nombre, Toast.LENGTH_LONG).show();
	                      
	                      bd.execSQL("DELETE FROM contacto WHERE nombre='"+nombre+"'");
	                      /*boolean resultado = deleteDatabase(nombreBD);
	                      if(resultado)*/ 
	                        Toast.makeText(getApplicationContext(), 
	                        	  rb.getString("contacto.eliminado.correctamente"), Toast.LENGTH_LONG).show();
	                        /*else 
	                          Toast.makeText(getApplicationContext(), 
	                        	  "No se ha podido eliminar la base de datos", Toast.LENGTH_LONG).show();            	  
	                  		*/
	                        Intent main = new Intent(Principal.this,Principal.class);
	      	        	  	startActivity(main);
	                  } 
	                  catch (Exception e) 
	                  {
	                    Toast.makeText(getApplicationContext(), 
	                  		  rb.getString("no.se.ha.podido.eliminar.al.contacto"), Toast.LENGTH_LONG).show();
	                  }
	                } 
	              }); 
	              alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() 
	              {
	                public void onClick(DialogInterface dialog, int which) 
	                {
	                  Toast.makeText(getApplicationContext(), 
	                  		rb.getString("eliminacion.de.contacto.cancelada"), Toast.LENGTH_LONG).show();
	                } 
	              }); 
	              alertDialog.show();
	              
	              Log.d(Constantes.DEBUG,"Fin -- onOptionsItemSelected()");
	              
	        	return true;
	        }
	        default:
	        	Log.d(Constantes.DEBUG,"Fin -- onOptionsItemSelected()");
	    		
	            return super.onOptionsItemSelected(item);
	    }
	    
	    
	}
	
	
	private String readFromBuffer(BufferedReader br){
		StringBuilder text = new StringBuilder();
		   try{
		      String line;
		      while ((line = br.readLine()) != null) {
		         text.append(line);
		         text.append("\n");
		      } 
		   } catch (IOException e) { 
		      e.printStackTrace();
		      // tratar excepci�n!!!
		   }
		   return text.toString();
    }
	
	private String filmProcess(String value, String searchText){
		
		StringTokenizer st = new StringTokenizer(value, searchText);
		
		//film .html
		while(st.hasMoreTokens()) {

			   String str = st.nextToken();
			   if(str.toLowerCase().contains("Ben")){
				   System.out.println("����������"+str+"!!!!!!!!!!");
				   System.out.println("-->"+st.nextToken());
			   }

			   
		}//fin while
		
		return null;
	}
}