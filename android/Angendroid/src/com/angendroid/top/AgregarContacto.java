package com.angendroid.top;

import java.util.ResourceBundle;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AgregarContacto extends Activity {

	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	EditText TextNombre;
	EditText TextTelefono;
	EditText TextCorreo;
	EditText TextDescripcion;
	private Button butGuardar;
	
	private SQLiteDatabase baseDatos;   
	private static final String TAG = "bdagenda";   
	private static final String nombreBD = "agenda";   
	private static final String tablaContacto = "contacto";  
	
	//guardamos en un String toda la creación de la tabla        
	  private static final String crearTablaContacto = "create table if not exists "  
		  + " contacto (codigo integer primary key autoincrement, "  
		  + " nombre text not null, telefono text not null unique, "
		  + " correo text not null, descripcion text not null);";

	//ResourceBundle para lectura de properties...
    ResourceBundle rb = ResourceBundle.getBundle("assets.appMessages");
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		Log.d(Constantes.DEBUG, "Inicio -- onCreate()");
		
		super.onCreate(savedInstanceState);
		// TODO Coloque su código aquí
		setContentView(R.layout.agregar);
		
		TextNombre = (EditText)findViewById(R.id.TextNombre);
		TextTelefono = (EditText)findViewById(R.id.TextTelefono);
		TextCorreo = (EditText)findViewById(R.id.TextCorreo);
		TextDescripcion = (EditText)findViewById(R.id.TextDescripcion);
					
		butGuardar = (Button)findViewById(R.id.butGuardar);
		
		//Guardar el contacto actual en la agenda
	    butGuardar.setOnClickListener(new View.OnClickListener() 
	    {
	      public void onClick(View v) 
	      {
	        //Abrir la base de datos, se crear� si no existe             		
	        abrirBasedatos();

	        //Insertar una fila o registro en la tabla "contacto"
	        //si la inserción es correcta devolverá true     
	        boolean resultado = insertarFila(
	        		TextNombre.getText().toString(), 
	        		TextTelefono.getText().toString(),
	        		TextCorreo.getText().toString(),
	        		TextDescripcion.getText().toString()
	        		);
	        if(resultado) 
	          Toast.makeText(
	        		  getApplicationContext(), 
	        		  rb.getString("contacto.anyadido.correctamente"),
	        		  Toast.LENGTH_LONG).show();
	        else 
	          Toast.makeText(
	        		  getApplicationContext(), 
	        		  rb.getString("no.se.ha.podido.guardar.el.contacto"),
	        		  Toast.LENGTH_LONG).show();      
	        
	        Intent main = new Intent(AgregarContacto.this,Principal.class);
    		startActivity(main);
	      }
	    });  
	    
	  //Procedimiento para abrir la base de datos
	    //si no existe se creará, también se creará la tabla contacto        
	    
	    Log.d(Constantes.DEBUG, "Fin -- onCreate()");
	    
	}
	
	private void abrirBasedatos() 
    {   
		
		Log.d(Constantes.DEBUG, "Inicio -- abrirBasedatos()");
		
	      try 
	      {   
	        baseDatos = openOrCreateDatabase(nombreBD, MODE_WORLD_WRITEABLE, null);   
	        baseDatos.execSQL(crearTablaContacto);   
	      }    
	      catch (Exception e)
	      {   
	        Log.i(Constantes.INFO,rb.getString("error.al.abrir.o.crear.la.base.de.datos") + e);   
	      }   
	      
	      Log.d(Constantes.DEBUG,"Fin -- abrirBasedatos()");
	      
    }  
   
    //Método que realiza la inserción de los datos en nuestra tabla contacto                  
    private boolean insertarFila(String nombre, String telefono, String correo, String descripcion) 
    {   
    	
    	Log.d(Constantes.DEBUG,"Inicio -- insertarFila()");
    	
      ContentValues values = new ContentValues();   
      values.put("nombre",nombre );   
      values.put("telefono", telefono);
      values.put("correo", correo);
      values.put("descripcion", descripcion);
      
      Toast.makeText(
    		  getApplicationContext(),
    		  "Nombre: " + nombre + ", " +"telefono: " + telefono + ", " + "correo: "+ correo + ", " + "descripcion: "+ descripcion, Toast.LENGTH_LONG).show();
      
      Log.d(Constantes.DEBUG,"Fin -- insertarFila()");
      
      return (baseDatos.insert(tablaContacto, null, values) > 0);   
    }
}
